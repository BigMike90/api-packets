import Utils.InvalidData;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;

/*creare un metodo per getNext(), prev(), getSize to fill lenghtpacket(), generateId()
 * il protocollo setta cmq il prossimo id, next<- quano sar? creato un altro packet inizier? ad essere
 * settato con il next id. Il nuovo packet quindi metter? in current id L'EX next, ed in prev quello passato
 * */
public class MikePacket{

	private final String version="Mike";//Mike = 4byte
	public static int id; //32 bit
	private static int idNext;
	private static int idPrev;
	private static int idNextOld;
	private String sourceAddress; //32bit
	private String destinationAddress;//32bit
	private byte [] data = new byte [4];// = new byte[4]; //32bit
	private int lengthPacket;//32 bit
	//public boolean defrag=false;


	//if is first
	private MikePacket(int prev, int currentId, int next, String sourceAddress, String destinationAddress, byte []data) throws IOException, InvalidData {
		id=currentId;
		idNext=next;
		idNextOld=idNext; //backup next
		idPrev=prev;
		this.sourceAddress=sourceAddress;
		this.destinationAddress=destinationAddress;
		this.data= Arrays.copyOf(data, data.length);
		//if (this.defrag)	new MikePacket(this.id, this.idNext, this.sourceAddress, this.destinationAddress,  this.tempData); //in temp data va il rimanente dei data[]
	}



	public byte [] getPacket() throws IOException, InvalidData {
		/*byte idBox[]={(byte) this.id,
						 (byte) this.idNext,
						 (byte) this.idPrev};*/
		//concat.write(idBox); //ids ognuno da 32bit

		//scrittura di  tutti i sotto array di byte creati in un unico array
		ByteArrayOutputStream concat= new ByteArrayOutputStream();
		concat.write(this.version.getBytes()); //32 byte
		concat.write(this.id);
		concat.write(this.idNext);
		concat.write(this.idPrev);
		concat.write(getSourceAddress()); //32bit gli address
		concat.write(getDestinationAddress());
		concat.write(this.data); //32 byte

		byte tmp[]= concat.toByteArray();
		this.lengthPacket= tmp.length;
		return tmp;
	}

	public int getLengthPackets(){
		return this.lengthPacket;
	}


	private static int generateId() {
		return (int)( Math.random()*1000 ) + (int)(new Date().getTime()/1000);
	}

	private byte [] getSourceAddress() throws InvalidData  {
		 try {
			InetAddress address= InetAddress.getByName(this.sourceAddress.toString());
			 return address.getAddress();

		 } catch (UnknownHostException e) {
			throw new InvalidData("Source address invalid on method gtByName");
		}

	}

	private byte[] getDestinationAddress() throws InvalidData {
		try {
			InetAddress address= InetAddress.getByName(this.destinationAddress.toString());
			return address.getAddress();

		} catch (UnknownHostException e) {
			throw new InvalidData("Destination address invalid on method getByName");
		}
	}


	public static LinkedList<MikePacket> encode(byte[] bigData, String sourceAddress, String destinationAddress) throws InvalidData, IOException {

		LinkedList<MikePacket> packets = new LinkedList<>();
		MikePacket mp = null;
		int i=0, length=0;

			while ( length<bigData.length) {
				byte tmp[]= new byte[4];
				tmp=(Arrays.copyOf(bigData,tmp.length)); //riempio fino a 4 byte
				length+=tmp.length;
				System.out.println("created new packets");

					if(i==0) {
						mp = new MikePacket(0, generateId(), generateId(), sourceAddress, destinationAddress, tmp );
					}
					else{
						mp= new MikePacket(id, idNext, generateId(), sourceAddress, destinationAddress, tmp);
						id=idNextOld;
					}

				packets.add(mp);
				i++;
		}

		return packets;
	}


	public static void deocode(byte [] bigData) throws InvalidData {
		/*
			TODO
				-copy test deoode, i< bigdata lent
				- bisogna riconoscere tutti idata all'interno del byte[]
				- oppure boh fare i calcoli di ogni pacchetto , forse ho una idea
				- mi creo una collection o un arrat ausiliare con dentro soltanto la LENGTH
				- ovviamente length di ogni pacchetto in modo da poterlo trattare singolarmente e poi append a string
				- nel main posso fare,             mp2.getLengthPackets()
				- la cosa importante e che SO che OGNI PACKETS E' 19 BYTE
				 */
		int _offset=15;
		ByteArrayOutputStream dataTmp= new ByteArrayOutputStream();

		int index= _offset;

		while ( index < bigData.length ){


			for(int i=0;i<4; i++) {//TODO si pu� provare a togliere index ++ manuale e metterlo come condition dentro il for

				if( index < bigData.length)
					dataTmp.write(bigData[index]);

				if( ++index < bigData.length)
					System.out.println();

				else throw new InvalidData("index outfBounds");

				System.out.println("index: "+index+" , off: "+_offset+" bigdata "+bigData[index]);
			}


			if(_offset+15 +3< bigData.length) {
				_offset+=15 + 3;
				index= _offset;

			}
			else break;

		}

		byte[] tmp= dataTmp.toByteArray();
		String s = new String(tmp);
		System.out.println("the value of data is "+s);
	}

	public static void testDecode(byte[] bigData){
		//take 7 bit to int -> Byte.toUnsignedInt(array[1]) & 0x7f
		//a bigendian is 2 byte then ->( Byte.toUnsignedInt(array[2]) << 8 ) | Byte.toUnsignedInt(array[3]);
		byte tmp[]=new byte[4];
		for (int i=15,j=0; (j<tmp.length && i<bigData.length) ; i++,j++)
			tmp[j]=bigData[i];

		String s = new String(tmp);
		System.out.println("The value of data is "+s);
	}

	//metodo creato per testing

	public static LinkedList<MikePacket> testEncode(byte[] bigData) throws IOException, InvalidData {
		LinkedList<MikePacket> packets = new LinkedList<>();
		MikePacket mp = new MikePacket(0, generateId(), generateId(), "127.0.0.1", "127.0.0.1", bigData);
		packets.add(mp);
		return packets;
	}


	public byte[] getData(){
		return this.data;
	}
}

