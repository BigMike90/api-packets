**The goal is to create a simple packet protocol like IP protocol.**
****
MikePackets.java is an api designed for java to send data (an example via sockets) with extra information of the packet, it was created with Intellij IDEA.
The information inside of packet are an string version, ID randomly generated, IDnext for the next packet ID, IDprev previus packet ID, source and destination address,
and a filed DATA of 4 byte to store the information to send.
When the information we want to send exceeds 4 byte a new packet is created with the remaining bytes and new IDs are generated. The goal is to send all packets in bytes 
to be universal with every programs that accepts data bytes.

***
**Methods important to use MikePackets**

Some important method that implements MikePackets is encode, decode and getPacket.

* Encode is a static method to create multiple packets. If the data to send exceeds than 4 bytes this method make a new packet with new info
and ends by returning a collection(a collection of total packets created). 

* getPacket is a method that return all information and fields of class MikePackets in a byte array, field data included.

* Decode is a method designed for receiving byte arrays. This metod give in input the byte array of packets for return the content of data. (fow now there are some bug here)

* encodeTest and decodeTest are destined only for testing becouse they work don't work for multiple packets but only one.

***
**Using MikePackets**

To better understand the mechanism of class i will report here some methods used in the main.
This snippet rapresents method encode/decode TEST and work only for 1 packet. 

Encode return a collection(packets, in this case only 1 packet), after i save this packet i use an internal method, getPacket(), it's return
a byte array of this packet so the array[] is ready to send. When this byte array has been received is ready to be decoded.

`   
    
     LinkedList<MikePacket> packets = MikePacket.testEncode(bigData);

     MikePacket mp=packets.getFirst();
     
     byte [] array = Arrays.copyOf(mp.getPacket(), mp.getLengthPackets());
     
     MikePacket.testDecode(array);
`

***

Here is reported the methods for using the class with multiple packets. As before i store the collection returned inside on 
byte array and now we're ready to send "array2", at receiving file need to decode from byte to data(string or other)

`
      
      LinkedList<MikePacket> packets2= MikePacket.encode(bigData, host,hostDestination);
      
      ByteArrayOutputStream concat= new ByteArrayOutputStream();

        for (MikePacket mp2: packets2){
        
            concat.write(mp2.getPacket());
            
        }
        
        byte[] array2= concat.toByteArray();

        MikePacket.deocode(array2);

`
