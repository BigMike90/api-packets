package Utils;

public class InvalidData extends Exception {//static maybe
	public InvalidData(String message) {
		super(message);
	}
	
	public InvalidData(String message, Throwable cause) {
		super(message,cause);
	}

}
