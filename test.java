import Utils.InvalidData;

import javax.xml.crypto.Data;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.LinkedList;

public class test{
    public static void main (String args[]) throws InvalidData, IOException, ClassNotFoundException {
        int port=1234;
        String host="localhost";
        byte [] bigData="12345678".getBytes();


     //with encode we create packets
     LinkedList<MikePacket> packets = MikePacket.testEncode(bigData);
     MikePacket mp=packets.getFirst();

     //with getpacket() we have all packet encoded in byte ready to send
     byte [] array = Arrays.copyOf(mp.getPacket(), mp.getLengthPackets());
     System.out.println("packets byte encoded ready to send: "+array.length+" byte");

     MikePacket.testDecode(array);

     // encoding with multiple packets
      LinkedList<MikePacket> packets2= MikePacket.encode(bigData, host,host);
      ByteArrayOutputStream concat= new ByteArrayOutputStream();

        for (MikePacket mp2: packets2){
            concat.write(mp2.getPacket());
        }
        byte[] array2= concat.toByteArray();

        System.out.println("******************************************************");
        System.out.println("packets byte encoded ready to send: "+array2.length+" byte");

        MikePacket.deocode(array2);

    }
}



//TODO    per quando faro l'invio socket
//  Socket s = new Socket(host, port);
//   //  DataOutputStream os = new DataOutputStream(s.getOutputStream());
//   //  os.writeInt(array.length);
//    // os.write(array);
// Per quando farò l'invio di pacchetti multipli ->   for (MikePacket mp : packets) os.write();